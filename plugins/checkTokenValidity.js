import axios from "axios";

export default async function({ store, redirect }) {
  if (store.getters["auth/isAuthenticated"]) {
    console.log("Verifying user");

    try {
      const validationResponse = await axios.get(
        "http://localhost:3000/user-verify",
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${store.state.auth.token}`
          }
        }
      );
      store.commit("auth/setUser", validationResponse.data.payload);
    } catch (error) {
      store.commit("auth/authError", 401);
    }
  } // console.log("PLUGIN 2")
}

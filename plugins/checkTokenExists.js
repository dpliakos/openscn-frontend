export default function({ store }) {
  if (process.browser) {
    const token = localStorage.getItem("token");
    console.log(token);
    if (token) {
      store.commit("auth/setTokenOnStartup", token);
    }
  }
}

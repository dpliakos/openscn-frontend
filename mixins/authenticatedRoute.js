export default {
  beforeCreate() {
    if (!this.$store.getters["auth/isAuthenticated"])
      this.$router.push("/login");
  }
};

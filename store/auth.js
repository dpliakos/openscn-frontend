import { resolve } from "path";
import { reject } from "q";

export const state = () => ({
  token: "",
  status: "",
  user: ""
});

export const getters = {
  isAuthenticated: state => !!state.token,
};

export const mutations = {
  authRequest(state) {
    state.status = "loading";
  },

  authSuccess(state, token) {
    state.status = "success";
    if (process.browser)
      localStorage.setItem("token", token)
    state.token = token;
  },

  setUser(state, user) {
    state.user = user;
  },

  authError(state, errorCode) {
    if (process.browser) {
      if (localStorage.getItem("token"))
        localStorage.removeItem("token")
    }
    state.token = ""
    state.user = ""
    state.status = errorCode;
  },

  setTokenOnStartup(state, token) {
    state.status = "success";
    state.token = token;
  },

  removeToken(state) {
    if (process.browser) {
      if (localStorage.getItem("token"))
        localStorage.removeItem("token")
    }
    state.token = "";
  }
};

export const actions = {
  login(context, user) {
    return new Promise((resolve, reject) => {
      context.commit("authRequest");
      this.$axios
        .post("http://localhost:3000/users/login", user, {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          const token = res.data.payload.token.substring(7);
          context.commit("authSuccess", token);
          const user = res.data.payload.user;
          context.commit("setUser", user);
          resolve(res);
        })
        .catch(err => {
          context.commit("authError", err.response.status);
          reject(err);
        });
    });
  },

  register(context, user) {
    return new Promise((resolve, reject) => {
      context.commit("authRequest");
      this.$axios
        .post("http://localhost:3000/users", user, {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          const token = res.data.payload.token.substring(7);
          context.commit("authSuccess", token);
          resolve(res);
        })
        .catch(err => {
          context.commit("authError", err.response.status);
          reject(err);
        });
    });
  },

  logout(context) {
    return new Promise((resolve, reject) => {
      context.commit("authRequest");
      this.$axios
        .post("http://localhost:3000/users/logout")
        .then(res => {
          context.commit("removeToken");
          resolve(res);
        })
        .catch(err => {
          context.commit("authError", err.response.status);
          reject(err);
        });
    });
  }
};

export const state = () => ({
  drawer: null,
  title: null
});

// mutations
export const mutations = {
  toggleDrawer(state) {
    state.drawer = !state.drawer;
  },

  setDrawer(state, drawer) {
    state.drawer = drawer;
  },

  setTitle(state, title) {
    state.title = title;
  }
};

export const getters = {
  drawer(state) {
    return state.drawer;
  }
};
